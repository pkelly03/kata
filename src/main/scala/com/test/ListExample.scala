package com.test

class ListExample() {

    def getLastElement(tempList: List[Int]) {
      println("Last element : " + tempList.last)
      tempList.last
    }

}
package com.test

import org.scalatest.Spec

class ListExampleSpec extends Spec {

  describe("Last Element In List") {
    it("should return the last element in the list") {
      val listExample = new ListExample
      val lastElement = listExample.getLastElement(List(1,3,4,10))
      println (" Last element returned : " + lastElement)
      assert(lastElement === 10)

    }
  }

}